package com.jfinal.weixin.sdk.utils;

import java.nio.charset.Charset;

/**
 * 字符集工具类
 * Author: L.cm
 * Date: 2016年3月29日 下午3:44:52
 */
public class Charsets {

	// 字符集utf-8
	public static final Charset UTF_8 = Charset.forName("utf-8");

}
